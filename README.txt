Что делаем сначала:
	1. Открываем cmd строку
	2. Вставляем туда это:
	@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command " [System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
	3. choco -v : Если выходит версия то все ок

Устанавливаем все программы:
	1. programs.txt && update programs.txt - делаешь из этих файлов .bat
	2. Запускаешь programs.bat потом update programs.bat
	3. Идешь в папочку "Install apps"
	4. Скачиваешь все приложения оттуда
	5. В папке "Icon For Labels" - находятся дополнительные иконки для ярлыков

Настраиваем систему:
	1. Открываем приложение Taskbar Groups
	2. Заходим в папку "UsedIcons"
	3. В папке "Apps" - находятся иконки для замены существующих ярлыков
	4. В папке "Groups" - находятся иконки для групп
	5. В папке "Label" - должны находится все ярлыки по группам

Что нужно сделать чтобы доделать настройку системы?
	Находим ярлыки программ, выносим их в нужную групповую папку "Label" и потом в программе "Taskbar Groups" ссылаемся на них
	ВАЖНО: ОБНОВИ ВСЕ ЯРЛЫКИ!!!

Чтобы расположить ярлыки "TaskbarGroups" в панеле задач нужно в панели задач кликнуть ПКМ - "Открыть папку" далее заходим в :C папку,
находим "TaskbarGroups" - "Shortcuts" забираем ярлыки отсюда - переносим их в Открытую папку панели задач

Настроиваем рабочую зону:
	1. Скачиваем Django, DRF
	2. Скачиваем Vue